#+TITLE: Settings for Emacs
~babel~
* Meta Appearance
** Line Numbers
 #+BEGIN_SRC emacs-lisp
 (use-package linum-relative
  :ensure t
  :config
  (linum-relative-global-mode))
 #+END_SRC

** Scrolling
#+BEGIN_SRC emacs-lisp
(use-package smooth-scrolling
  :ensure t
  :config
  (smooth-scrolling-mode 1))
#+END_SRC

** Bars
#+BEGIN_SRC emacs-lisp
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1) 
#+END_SRC

** Formatting
#+BEGIN_SRC emacs-lisp
  (use-package smartparens
    :ensure t
    :diminish smartparens-mode
    :config
    (add-hook 'prog-mode-hook 'smartparens-mode))

 (use-package aggressive-indent
	      :ensure t)
(add-hook 'prog-mode-hook 'electric-pair-mode)
#+END_SRC

* Behavior
** Misc
#+BEGIN_SRC emacs-lisp
(setq backup-inhibited t)
;disable auto save
(setq auto-save-default nil)

(global-visual-line-mode t)

(setq gc-cons-threshold 64000000)
(add-hook 'after-init-hook #'(lambda ()
                               ;; restore after startup
                               (setq gc-cons-threshold 800000)))
#+END_SRC

** Yes or No
#+BEGIN_SRC emacs-lisp
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC

** dashbord

#+BEGIN_SRC emacs-lisp
(use-package dashboard
    :ensure t
    :diminish dashboard-mode
    :config
    (setq dashboard-banner-logo-title "I bless your computer my child")
    (setq dashboard-startup-banner "~/.emacs.d/images/kisspng-richard-stallman-computer-software-programmer-gnu-emacs-5b2613cdd883c7.3959239615292220938869.png")
    (setq dashboard-center-content t)
    (add-to-list 'dashboard-items '(agenda) t)
    (setq dashboard-items '((recents  . 5)
                            (bookmarks . 0)))
    (dashboard-setup-startup-hook))
#+END_SRC

** treemacs
#+BEGIN_SRC emacs-lisp
(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :config
 (treemacs-icons-dired-mode)
)
#+END_SRC

* Git
#+BEGIN_SRC emacs-lisp
  (use-package magit
    :ensure t
    :bind ("C-x g" . magit-status))

  (use-package git-gutter
    :ensure t
    :config
    (global-git-gutter-mode 't)
    :diminish git-gutter-mode)

  (use-package git-timemachine
	       :ensure t)
#+END_SRC

* Langauges
#+BEGIN_SRC emacs-lisp
(use-package lsp-mode
  :hook (lsp-mode . lsp)
  :commands lsp)
#+END_SRC

* Theme
** Syntax Theme
#+BEGIN_SRC emacs-lisp
(use-package doom-themes
  :init
  (load-theme 'doom-palenight t)
  :config
  (progn
    (doom-themes-neotree-config)
    (setq doom-neotree-line-spacing 0)
    (doom-themes-org-config)))
#+END_SRC
** modeline
#+BEGIN_SRC emacs-lisp

(use-package doom-modeline
      :ensure t
      :hook (after-init . doom-modeline-mode)
)
#+END_SRC
