(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
'("melpa" . "https://melpa.org/packages/"))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
(package-refresh-contents)
(package-install 'use-package))

(use-package try
:ensure t)

(add-hook 'org-mode-hook 'org-indent-mode)

(setq org-image-actual-width 'nil)

(use-package org-bullets
:ensure t
:init
(setq org-bullets-bullet-list
'("◎" "◉" "⚫" "-"))
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(require 'org) 
(org-babel-load-file (expand-file-name (concat user-emacs-directory "settings.org")))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0" "2642a1b7f53b9bb34c7f1e032d2098c852811ec2881eec2dc8cc07be004e45a0" default)))
 '(package-selected-packages
   (quote
    (one-themes atom-dark-theme telephone-line irony elpy go-mode markdown-mode swift-mode web-mode git-timemachine git-gutter magit dashboard aggressive-indent rainbow-mode rainbow-delimiters smartparens smooth-scrolling linum-relative org-bullets try use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
